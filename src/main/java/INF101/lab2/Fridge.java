package INF101.lab2;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.ArrayList;

public class Fridge implements IFridge{
     
    public ArrayList<FridgeItem> items = new ArrayList<FridgeItem>(); 
    public int size = 20; 


    public Fridge(){
        this.items = new ArrayList<FridgeItem>();
        this.size = 20; 

    }
   


   public int nItemsInFridge(){
       return items.size(); 

   }

   public int totalSize() {
       return size;
   }
  
   public boolean placeIn(FridgeItem item) {
       if(items.size() < 20){
            items.add(item); 
            return true;
       } else{
           return false;
       }
   }

   public void takeOut(FridgeItem item){
    if(items.contains(item)){items.remove(item);}
    else{throw new NoSuchElementException();}
    }

   public void emptyFridge(){
       items.removeAll(items);

   }


   public List<FridgeItem> removeExpiredFood() {
       ArrayList<FridgeItem> expiredFood = new ArrayList<>();
       for(int i=0; i < nItemsInFridge(); i++){
           FridgeItem item = items.get(i);
           if(item.hasExpired()) {
               expiredFood.add(item);
           }
       }
       for(FridgeItem expiredItem : expiredFood){
           items.remove(expiredItem);
       }
       return expiredFood;
   }
}
